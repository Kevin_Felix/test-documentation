<img width="300px" src="https://github.com/rafinskipg/git-changelog/raw/master/images/git-changelog-logo.png" />

# Git Changelog

_Git changelog is a utility tool for generating changelogs. It is free and opensource. :)_

## v1.0.0  


### Documentation

  - **readme**
    - Add documentation for explaining the commit message
  ([1e41f7b6](https://gitlab.com/Kevin_Felix/test-documentation/commit/1e41f7b61ebf8443ae6c5901d8696434b9a03b02))
    - Add documentation for explaining the commit message
  ([502de0f9](https://gitlab.com/Kevin_Felix/test-documentation/commit/502de0f999261b8abfb7714a94efdf24d0c1a6cd))
    - Add documentation for explaining the commit message
  ([56b1e2d4](https://gitlab.com/Kevin_Felix/test-documentation/commit/56b1e2d48a9cdb1337a5cb0d275a5d3cd7e30a7a))




### Changed
  - Change .changelogrc
  ([42f8be0f](https://gitlab.com/Kevin_Felix/test-documentation/commit/42f8be0f85da880f136ee77490faf353127b985b))





---
<sub><sup>*Generated with [git-changelog](https://github.com/rafinskipg/git-changelog). If you have any problems or suggestions, create an issue.* :) **Thanks** </sub></sup>
