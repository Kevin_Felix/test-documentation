Dokumentasi Apps
=====================

Description
--------------------------
This project creates full-stack platform-specific downloadable packages for GitLab.
For other installation options please see the
GitLab installation page.

Getting Started
--------------------------
### Prerequisites

What things you need to install the software and how to install them
- Ubuntu/Debian/CentOS/RHEL/OpenSUSE
- Ruby (MRI) 2.6.3
- Git 2.8.4+
- Redis 2.8+
- PostgreSQL (preferred) or MySQL

For more information please see the [architecture documentation.](https://www.google.co.uk/)

### Installing

A step by step series of examples that tell you how to install it

Clone our repo :
```r
# Use git, with HTTPS
git clone https://gitlab.com/Kevin_Felix/test-documentation.git
```
or
```r
# Use git, with SSH
git clone git@gitlab.com:Kevin_Felix/test-documentation.git
```
Set auto changelog :
```r
npm install -g git-changelog
```
or
```r
npm install git-changelog --save-dev
```
Create a file with name .changelogrc then add into it :

End of installing demo

### Guest Usage Guide
A step by step series of examples that tell you how to run this system
```r
# Give the example
Give the example
```
End with an example of getting some data out of the system or using it for a little demo

Other Documentation
--------------------------
- All documentation can be found on [here.](doc.gitlab.com/ce/)
- The documentation overview is in the [readme in the doc directory.](https://www.google.co.uk/)

License
--------------------------
See the [LICENSE](./LICENSE) file for licensing information as it pertains to
files in this repository.

Getting help
--------------------------
Please see [Getting help for GitLab](https://www.google.co.uk/) on our website for the many options to get help.

