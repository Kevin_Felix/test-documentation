#!/bin/bash
# Generates changelog day by day
echo "CHANGELOG"
echo ----------------------
git log --tags --format="%cd %d" --date=short | sort -u -r -d | while read DATE VERSION ; do
    echo
    echo [$DATE]
    echo [$VERSION]
    GIT_PAGER=cat git log --format=" * %cn committed %h - message : %s" --since="$DATE 00:00:00" --until="$DATE 24:00:00"
done > CHANGELOG.md
git add CHANGELOG.md
